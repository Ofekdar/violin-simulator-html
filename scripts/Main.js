var selected = "None";
		
function sleep(milliseconds) // thanks to https://www.sitepoint.com/delay-sleep-pause-wait/ for the function
{
  const date = Date.now();
  let currentDate = null;
  do 
  {
	currentDate = Date.now();
  }
  while (currentDate - date < milliseconds);
}
		
function selectNote(id)
{
	selected = id;
	
	halfNote.style.border = "2px solid #000";
	wholeNote.style.border = "2px solid #000";
	quarterNote.style.border = "2px solid #000";
	eighthNote.style.border = "2px solid #000";
	halfBreak.style.border = "2px solid #000";
	wholeBreak.style.border = "2px solid #000";
	quarterBreak.style.border = "2px solid #000";
	eighthBreak.style.border = "2px solid #000";
	removeNote.style.border = "2px solid #000";
	document.getElementById(id).style.border = "2px solid #fa0";
			
			
	return false;
}
		
function placeNote(event)
{
	//alert(event.clientX + "   " + event.clientY + "   " + selected + "     " + event.button);
	if(selected != "None" && event.button == 0)
	{
		var totalXPos = ((event.clientX - 113) / 64.5) | 0;
		var totalYPos = ((event.clientY - 53) / 42.5) | 0;
		
		if(selected.indexOf("removeNote") == -1)
		{
			document.getElementById("Note" + totalXPos).src = ("pictures/" + (selected.indexOf("Break") == -1 ? "placedNotes/" : "placedBreaks/") + selected + ".jpg");
		
			document.getElementById("Note" + totalXPos).style.display = 'block';
			document.getElementById("Note" + totalXPos).style.left = (113 + totalXPos * 64.25)+"px";
			document.getElementById("Note" + totalXPos).style.top = (53 + totalYPos * 42.5)+"px";
		}
		else
		{
			document.getElementById("Note" + totalXPos).style.display = "none";
			document.getElementById("Note" + totalXPos).src = "";
		}
	}
}

	
function playNotes(event)
{
	if(selected != "None" && event.button == 0)
	{
	/*
		function disableMouse(e)
		{
			e.preventDefault();
			e.stopPropagation();
		}
		document.addEventListener("click", disableMouse, true);*/
	/*
		document.addEventListener("click", function(e)
		{
			e.preventDefault();
			e.stopPropagation();
		} , true);
		*/
		
		var track = 1;
		
		function moveOnRows(i)
		{
			document.getElementById("track" + (12 - track)).pause();
			
			if(i > 0)
			{ 
				if((document.getElementById("Note" + (i - 1)).src).indexOf("Note") != -1)
				{
					track = ((((parseInt((document.getElementById("Note" + (i - 1)).style.top).substring(0, ((document.getElementById("Note" + (i - 1)).style.top).length) - 2)) - 53) / 42) | 0) + 1);
					document.getElementById("track" + (12 - track)).currentTime = 0; 
					document.getElementById("track" + (12 - track)).play(); 
				}
				if((document.getElementById("Note" + (i - 1)).src).indexOf("eighth") != -1)
				{	
					sleep(300);
				}
				else if((document.getElementById("Note" + (i - 1)).src).indexOf("quarter") != -1)
				{	
					sleep(600);
				}
				else if((document.getElementById("Note" + (i - 1)).src).indexOf("half") != -1)
				{	
					sleep(1200);
				}
				else if((document.getElementById("Note" + (i - 1)).src).indexOf("whole") != -1)
				{	
					sleep(2400);
				}
				document.getElementById("colom" + (i - 1)).style.backgroundColor = 'transparent';
			}
			
			if(i < 9)
			{
				document.getElementById("colom" + i).style.backgroundColor = '#f92';
			}
			else
			{
				document.getElementById("track" + (12 - track)).pause(); 
				document.getElementById("track" + (12 - track)).currentTime = 0;
			}
					
			i++;
			if (i < 10) window.setTimeout(function(){ moveOnRows(i); }, 0); // credit: https://stackoverflow.com/questions/5136960/change-document-before-a-function-ends
		}
		moveOnRows(0);
	}
	return 0;
}

document.addEventListener("click", printMousePos);
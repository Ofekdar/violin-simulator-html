﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8" />
        <title>Contact Us</title>

        <style>
            h3
            {
                margin-bottom: 0.6em;
                margin-top: 0.6em;
            }
            input
            {
                margin-bottom: 1em;
                margin-left: 1.5em;
            }
        </style>
    </head>
    <body style="background-color:powderblue;">

        <div style="position: absolute; left: 290px; top: 15px; text-align: center;">
            <u><h1>Contact Page</h1></u>
            <h3>Please in the form below, specifi the problem you had;</h3>
            <h3>If you can add a file showing the problem, that will help us define and fix the problem faster;</h3>
            <h3 style="color: #ee4200">Please note the warnings mentioned at the "Home" page under the "Help & Support" tab.</h3>
        </div>

        <form style="position: absolute; left: 140px; top: 215px;">
            <input type="file" name="attachedfile" >
            <textarea id = "userProblem" rows = "20" cols = "152"  placeholder="Tell us about the problem..."></textarea>
            <p><input type="submit" value="Submit"/>
            <input type="reset" value="Reset"/></p>
        </form>

        <!-- Button to go back to Home page -->
        <button style="position: absolute; top: 10px; left: 10px; height:30px; width:150px; opacity: 0.5; border: 2px solid #ff6a00; border-radius: 100px; outline: none; cursor: pointer; font-size: 14px; font-family: 'Comic Sans MS', cursive, sans-serif;" onclick="window.location.href = 'Home Page.aspx';">< Home Page</button>
    </body>
</html>

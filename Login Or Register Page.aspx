﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8" />

        <link rel="icon" href="icons/Login.ico">

        <title>Register / Login</title>   

        <script Language="javascript" src="scripts/Login.js"></script>

        <link rel="stylesheet" type="text/css" href="styles/LoginStyles.css">

        <style>
            .infoPic
            {
                margin-top: 0.6em;
                margin-bottom: 0.85em;
                cursor: pointer;
            }
        </style>
            
    </head>
    <body background="pictures/others/LoginBackgroundPic.jpg"> <!-- Background image -->
    
        <!-- The background rectengles -->
        <svg style="position: absolute; top: 55px; left: 175px;" width="975" height="550">
          <rect rx="40" ry="40" width="950" height="550" style="fill:rgb(23, 23, 23);" />
        </svg>
        <svg style="position: absolute; top: 80px; left: 200px;" width="1000" height="500">
          <rect rx="20" ry="20" width="900" height="500" style="fill:rgb(243, 243, 238);" />
        </svg>

        <!-- Login form -->
        <form style="position: absolute; text-align: center; top: 120px; left: 370px; action="mysuperscript.php" autocomplete="on"> 
		    <h1>Log in</h1> 
		    <p> <input id="username" name="username" required="required" type="text" placeholder="Username"/> </p>
		    <p> <input id="password" name="password" required="required" type="password" placeholder="Password"/> </p>
		    <p class="login button"> <input type="submit" value="Login" /> </p>
	    </form>

        <!-- Register form -->
        <form style="position: absolute; text-align: center; top: 120px; left: 730px; action="mysuperscript.php" autocomplete="on" onsubmit="return validate();"> 
		    <h1> Sign up </h1> 
	        <p> <input id="usernamesignup" name="usernamesignup" required="required" type="text" placeholder="Username"/> </p>
		    <p> <input id="passwordsignup" name="passwordsignup" required="required" type="password" placeholder="Password"/> </p>
		    <p> <input id="passwordsignup_confirm" name="passwordsignup_confirm" required="required" type="password" placeholder="Confirm Password"/> </p>
            <label>Favorite color:</label>
            <input type="color" id="favoritecolor" name="favoritecolor" value="#ff0000">
		    <p class="signin button"> <input type="submit" value="Sign up"/> </p>
	    </form>

        <!-- Pop-ups, credit to w3school https://www.w3schools.com/howto/howto_js_popup.asp -->
        <div class="popup"> 
            <p class="infoPic" onClick="showPopup('username')"> <img alt="Info" src="pictures/others/Register Info Icon.png" width="20" height="20"/> </p>
            <p class="infoPic" onClick="showPopup('password')"> <img alt="Info" src="pictures/others/Register Info Icon.png" width="20" height="20"/> </p>
            <p class="infoPic" onClick="showPopup('rePassword')"> <img alt="Info" src="pictures/others/Register Info Icon.png" width="20" height="20"/> </p>
            <p class="infoPic" onClick="showPopup('color')"> <img alt="Info" src="pictures/others/Register Info Icon.png" width="20" height="20"/> </p>
            <span class="popuptext" id="myPopup">yeet</span>
        </div>

        <!-- "Continue Anonymously" button -->
        <div style="position: absolute; top: 420px; left: 480px; text-align: center;">
            <button style="height:40px; width:300px; border: 2px solid #1d2680; border-radius: 100px; outline: none; cursor: pointer; font-size: 20px; font-family: 'Comic Sans MS', cursive, sans-serif;" onclick="window.location.href = 'Main Page.aspx';">Continue Anonymously</button>
            <p style="color: gray">You won't be able to save your work<br />You will appear as "Anonymous" in chat</p> <!-- warning text below -->
        </div>
        
        <!-- Button to go back to home page -->
        <button style="position: absolute; top: 90px; left: 210px; height:30px; width:100px; opacity: 0.5; border: 2px solid #ff6a00; border-radius: 100px; outline: none; cursor: pointer; font-size: 14px; font-family: 'Comic Sans MS', cursive, sans-serif;" onclick="window.location.href = 'Home Page.aspx';">< Home Page</button>

    </body>
</html>

<%@ Page Language="C#" %>

<!DOCTYPE html>
<html>
	<head>

        <link rel="icon" href="icons/Home.ico">

        <title>Violin Simulator</title>

        <script Language="javascript" src="scripts/Home.js"></script>
        
        <link rel="stylesheet" type="text/css" href="styles/HomeStyles.css">

        <style>
             h3
            {
                margin-bottom: 0.6em;
            }
        </style>
	</head>
    <body background="pictures/others/LoginBackgroundPic.jpg"> <!-- Background image -->
        
        <!-- The background rectengles -->
        <svg style="position: absolute; top: 90px; left: 60px;" width="1250" height="500">
          <rect rx="40" ry="40" width="1250" height="500" style="fill:white;" />
          <rect width="40" height="40" style="fill:white;" />
        </svg>
        
        <!-- The tabs' buttons -->
        <div>
          <button class="tablinks" style="left: 60px;" onclick="openCity(event, 'Home')" id="defaultOpen">Home</button>
          <button class="tablinks" style="left: 145px;" onclick="openCity(event, 'Info')">Info</button>
          <button class="tablinks" style="left: 215px;" onclick="openCity(event, 'Support')">Help & Support</button>
          <button class="tablinks" style="left: 353px;" onclick="openCity(event, 'Updates')">Updates</button>
        </div>

        <!-- The "Home" tab -->
        <div id="Home" class="tabcontent" style="left: 100px; top: 150px;">
          <img style="position: absolute; top: -50px; left: 870px;" alt="Violin" src="pictures/others/violin.jpg" width="286" height="450"/>
          <u><h2>Welcome!</h2></u>
          <p>Welcome to "Violin Simulator" by Ofek Daraby!</p>
          <p>Please read the "Info" tab if you are new to this site.</p>
          <p>You are free to explore this site and play with the tools it provides.</p>         
          <p style="display: table-cell; width: 800px;"><a style="color: #0094ff; text-transform: uppercase; text-decoration: none; letter-spacing: 0.15em; display: inline-block; padding: 15px 20px; position: relative;" href="Login Or Register Page.aspx">Go To The Violin</a></p>
        </div>
        
        <!-- The "Info" tab -->
        <div id="Info" class="tabcontent" style="left: 150px;">
          <u><h3>Information Center</h3></u>
          <p>This site provides a simple "Violin Player" tool in order to allow people who are new to<br />the violin to experience it without needing to commit to anything or people who just want<br />to play some sounds and have fun.</p>
          <p>If you want, you can Sign Up (or Log In if you already have an account)<br />and enjoy some more features that this site can provide.</p>
          <p>You can chat and connect with other people using the global chat that keeps updating.</p>
          <p><span style="color:#ff2700;">Please do NOT abuse or curse in the chat.</span> Just try to have some fun.</p>
          <p>If you would like to report a bug / person or even ask a staff anything, you are welcome<br />to contact us. More information about that is located at the "Help & Support" tab.</p>
        </div>
        
        <!-- The "Help & Support" tab -->
        <div id="Support" class="tabcontent" style="left: 185px;">
          <u><h3>Support & Contact</h3></u>
          <p>If you would like to report a bug or report a person for abusing or cursing,<br />please press the button below to go to a page where you can submit your request.</p>
          <p>We will try to handle your problem as soon as possible. All staff can see all<br />requests that are being submitted.</p>
          <p style="color:#808080; font-size:20px;">Please note that false hackusations or spam can lead to kicks/bans and even account deletion.</p>
          <p style="color:#808080; font-size:20px;">If you are planning to report, it's best to screenshot evidence to help us define the</br>problem and solve it but it's not a requirement.</p>
          <button style="margin-top: 1em; height:65px; width:250px; border: 2px solid #ff4600; border-radius: 100px; outline: none; cursor: pointer; font-size: 30px; font-family: 'Comic Sans MS', cursive, sans-serif;" onclick="window.location.href = 'Contact Page.aspx';">Contact Us</button>
        </div>
        
        <!-- The "Updates" tab -->
        <div id="Updates" class="tabcontent" style="left: 70px; text-align:left;">
          <u><h3>Latest Updates:</h3></u>
          <h4>This is a list of the latest updates:</h4>
          <ul>
            <li> 17/04/2020  -  Added "Contact Us" page (1.0.3)</li>
            <li> 16/04/2020  -  Added Buttons In Pages To Help Navigate Between Them (1.0.2)</li>
            <li> 16/04/2020  -  Added Info Pop-Ups for Sign Up Fields (1.0.2)</li>
            <li> 16/04/2020  -  Added Validation And "Favorite Color" Option To Sign Up Form (1.0.2)</li>
            <li> 12/04/2020  -  Added "Updates" Tab (1.0.1)</li>
            <li> 08/04/2020  -  Added Home Page (1.0.0)</li>
          </ul>
        </div>
        
        <!-- The version text -->
        <pre style="position: absolute; left: 1265px; top: 0px; font-size: 12px; color: white;">V   1 . 0 . 3</pre>

        <!-- Has to be here because button needs to be loaded first -->
        <script Language="javascript"> document.getElementById("defaultOpen").click(); </script>

    </body>
</html>
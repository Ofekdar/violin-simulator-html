function validate()
{
	return validateUsername() && validatePassword() && validateRePassword() && validateColor();
}

function validateUsername()
{
	var username = document.getElementById("usernamesignup");
	var errorMsg = username.value.length < 4 ? "The username must be at least 4 characters long." : username.value.length > 20 ? "The username entered is too long! max characters are 20." : !/^([a-zA-Z0-9_\.\-])+$/.test(username.value) ? "Username must be made from only a-z, A-Z, 0-9, \'-\', \'_\' or \'.\'." : !/[a-zA-Z]/.test(username.value) ? "Username must contain at least one letter." : "";
	if(errorMsg != "")
	{
		alert(errorMsg);
		return false;
	}   
	return true;             
}

function validatePassword()
{
	var password = document.getElementById("passwordsignup");
	var errorMsg = password.value.length < 6 ? "The password must be at least 6 characters long." : !/^([a-zA-Z0-9_\.\-])+$/.test(password.value) ? "Password must be made from only a-z, A-Z, 0-9, \'-\', \'_\' or \'.\'." : !/[a-zA-Z]/.test(password.value) && !/\d/.test(password.value) ? "Password must contain at least one letter and one digit." : "";
	if(errorMsg != "")
	{
		alert(errorMsg);
		return false;
	}   
	return true;            
}

function validateRePassword()
{
	if(document.getElementById("passwordsignup").value != document.getElementById("passwordsignup_confirm").value)
	{
		alert("The password in the confirm box is different then the one entered");
		return false;
	}
	return true;
}

function validateColor()
{
	var hex = (document.getElementById("favoritecolor").value).toString(16); // thanks to "Tim Down" for code : https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	if(parseInt(parseInt(result[1], 16)) >= 100 && parseInt(parseInt(result[2], 16)) >= 100 && parseInt(parseInt(result[3], 16)) >= 100)
	{
		alert("Color chosen is too bright! Make sure the color has at least 1 rgb value lower then 100.");
		return false;
	}
	return true;
}

function showPopup(field)
{
	var popup = document.getElementById("myPopup");
	popup.classList.toggle("show");

	popup.innerHTML = field == "username" ? ("The name displayed in chat.\n\n- 4 to 20 Characters\n- a-z , A-Z , 0-9\n- \'_\' , \'.\' , \'-\'\n- At least 1 letter").replace(/\n/g, "<br />") : field == "password" ? ("- At least 6 characters\n- a-z , A-Z , 0-9\n- \'_\' , \'.\' , \'-\'\n- At least 1 letter\n- At least 1 digit").replace(/\n/g, "<br />") : field == "rePassword" ? ("To avoid misspelling the password\n\n- Must be identical to the \"Password\" field").replace(/\n/g, "<br />") : ("Color of your username in chat\n\n- One of the rgb values must be below 100 (to avoid usernames that are too bright)").replace(/\n/g, "<br />");
}
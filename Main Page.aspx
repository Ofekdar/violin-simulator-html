﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html>
	<head>
	
		<meta charset="utf-8" />

        <link rel="icon" href="icons/Main.ico">
		
		<title>Violin Simulator</title>

        <script Language="javascript" src="scripts/Main.js"></script>
       
		<meta property="og:image" content="https://www.w3schools.com/images/w3schools_logo.png">
        
        <!-- <link rel="stylesheet" type="text/css" href="styles/MainStyles.css"> -->
		
		<style>
            .notes
            {
	            height:28px;
	            width:43px;
	            border: 2px solid #000;
	            border-radius: 100px;
	            padding: 5px;
	            display: block;
	            margin-left: 60px;
	            margin-top: 20px;
            }

            .coloms
            {
	            position: absolute;
	            top: 52px;
	            padding: 232px 30px;
	            cursor: pointer;
	            background-color: transparent;
	            border: none;
	            outline: none;
	            opacity: 0.5;
            }

            .linesNotes
            {
	            display: none;
	            position: absolute;
            }

            .chat
            {
	            position: absolute;
	            right: 150px;
	            top: 150px;
            }
		</style>
	</head>
	<body style="background-color:powderblue;">
	
		<!-- Audio tracks -->
		<audio id="track1"><source src="sounds/1.mp3" type="audio/mpeg"></audio>
		<audio id="track2"><source src="sounds/2.mp3" type="audio/mpeg"></audio>
		<audio id="track3"><source src="sounds/3.mp3" type="audio/mpeg"></audio>
		<audio id="track4"><source src="sounds/4.mp3" type="audio/mpeg"></audio>
		<audio id="track5"><source src="sounds/5.mp3" type="audio/mpeg"></audio>
		<audio id="track6"><source src="sounds/6.mp3" type="audio/mpeg"></audio>
		<audio id="track7"><source src="sounds/7.mp3" type="audio/mpeg"></audio>
		<audio id="track8"><source src="sounds/8.mp3" type="audio/mpeg"></audio>
		<audio id="track9"><source src="sounds/9.mp3" type="audio/mpeg"></audio>
		<audio id="track10"><source src="sounds/10.mp3" type="audio/mpeg"></audio>
		<audio id="track11"><source src="sounds/11.mp3" type="audio/mpeg"></audio>
		
		<!-- Notes to place -->
		<img class="linesNotes" id="Note0" src=""/>
		<img class="linesNotes" id="Note1" src=""/>
		<img class="linesNotes" id="Note2" src=""/>
		<img class="linesNotes" id="Note3" src=""/>
		<img class="linesNotes" id="Note4" src=""/>
		<img class="linesNotes" id="Note5" src=""/>
		<img class="linesNotes" id="Note6" src=""/>
		<img class="linesNotes" id="Note7" src=""/>
		<img class="linesNotes" id="Note8" src=""/>
		
		<!-- Buttons for each colom -->
		<button id="colom0" class="coloms" style="left: 114px;" onmousedown="placeNote(event)"></button>
		<button id="colom1" class="coloms" style="left: 177.5px;" onmousedown="placeNote(event)"></button>
		<button id="colom2" class="coloms" style="left: 241px;" onmousedown="placeNote(event)"></button>
		<button id="colom3" class="coloms" style="left: 305.5px;" onmousedown="placeNote(event)"></button>
		<button id="colom4" class="coloms" style="left: 370px;" onmousedown="placeNote(event)"></button>
		<button id="colom5" class="coloms" style="left: 434.5px;" onmousedown="placeNote(event)"></button>
		<button id="colom6" class="coloms" style="left: 499px;" onmousedown="placeNote(event)"></button>
		<button id="colom7" class="coloms" style="left: 562.5px;" onmousedown="placeNote(event)"></button>
		<button id="colom8" class="coloms" style="left: 626px;" onmousedown="placeNote(event)"></button>
		
		<!-- The selectable notes' buttons -->
		<div style="position: absolute; top: 150px; left: 670px;">
			<img id="wholeNote" class="notes" alt="" src="pictures/notes/wholeNote.jpg" onclick="selectNote(id)" />
			<img id="halfNote" class="notes" alt="" src="pictures/notes/halfNote.jpg" onclick="selectNote(id)" />
			<img id="quarterNote" class="notes" alt="" src="pictures/notes/quarterNote.jpg" onclick="selectNote(id)" />
			<img id="eighthNote" class="notes" alt="" src="pictures/notes/eighthNote.jpg" onclick="selectNote(id)" />
		</div>
		
		<!-- The selectable breaks' buttons -->
		<div style="position: absolute; top: 150px; left: 760px;">
			<img id="wholeBreak" class="notes" alt="" src="pictures/breaks/wholeBreak.jpg" onclick="selectNote(id)" />
			<img id="halfBreak" class="notes" alt="" src="pictures/breaks/halfBreak.jpg" onclick="selectNote(id)" />
			<img id="quarterBreak" class="notes" alt="" src="pictures/breaks/quarterBreak.jpg" onclick="selectNote(id)" />
			<img id="eighthBreak" class="notes" alt="" src="pictures/breaks/eighthBreak.jpg" onclick="selectNote(id)" />
		</div>
		
		<!-- The tool to remove placed notes/breakes -->
		<img id="removeNote" style="position: absolute; top: 430px; left: 715px;" class="notes" alt="" src="pictures/others/removeNote.jpg" onclick="selectNote(id)" />
		
		<!-- Letters near the notes table -->
		<b> <p style="position: absolute; top: 24px; left: 20px; line-height: 1.72; font-size: 25px; display: block; margin-left: 60px;" class="letters">G<br>F<br>E<br>D<br>C<br>B<br>A<br>G<br>F<br>E<br>D</p> </b>
		
		<!-- The notes table -->
		<img style="position: absolute; z-index: -1; top: 50px; left: 110px;" alt="placeable lines" src="pictures/others/notesTable.png" width="580" height="470"/>
		
		<!-- The "Play!" button -->
		<button style="position: absolute; top: 545px; left: 345px; height:60px; width:116px; border: 4px solid #050; border-radius: 100px; outline: none; cursor: pointer; font-size: 28px; font-family: 'Comic Sans MS', cursive, sans-serif;" onmousedown="playNotes(event, 1)">Play!</button>
		
		<!-- Global chat -->
		<h2 style="position: absolute; right: 100px; top: 70px; font-size: 32px; font-family: 'Comic Sans MS', cursive, sans-serif;">Global Chat:</h2>
		
		<!-- Button to go back to the login or register page -->
        <button style="position: absolute; top: 10px; left: 10px; height:30px; width:150px; opacity: 0.5; border: 2px solid #ff6a00; border-radius: 100px; outline: none; cursor: pointer; font-size: 14px; font-family: 'Comic Sans MS', cursive, sans-serif;" onclick="window.location.href = 'Login Or Register Page.aspx';">< Register / Login</button>
		
	</body>
</html>